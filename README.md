Tag Extension for Mecha
=======================

Release Notes
-------------

### 1.9.6

 - Updated for Mecha 2.4.0.

### 1.9.5

 - Added `page` property to `Tags` to store the parent page instance.

### 1.9.4

 - Updated for Mecha 2.2.2.

### 1.9.3

 - Fixed missing “Tag” title.
 - Fixed bug for tags page.

### 1.9.2

 - Added `$tag` variable to global.
